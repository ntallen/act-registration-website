/**
 * JS Object used to store the last clicked checkbox for each set of checkboxes. See method below
 * for usage details.
 */
var lastClickedIndexes = new Object();

/**
 * If called by a checkbox when the checkbox is clicked, will manage the
 * selection/deselection of a range of checkboxes when the Shift key
 * is held down during the click.
 *
 * @param clickedCheckbox - the checkbox originating the event
 * @param event - the onclick event itself
 */
function handleCheckboxRangeSelection(clickedCheckbox, event) {
    // Find the position of the checkbox in the array of checkboxes
    var checkBoxes = document.getElementsByName(clickedCheckbox.name);
    var clickedIndex;

    for (var i=0; i<checkBoxes.length; i++) {
        if (clickedCheckbox.value == checkBoxes[i].value) {
            clickedIndex = i;
            break;
        }
    }

    // Fetch the index of the last clicked checkbox for this set of checkboxes
    var lastClickedIndex = lastClickedIndexes[clickedCheckbox.name];

    // If the shift key was pressed, and we have a previously clicked
    // checkbox, "click" the whole range
    if ((event.shiftKey == true) && (lastClickedIndex != null))  {
        var startId = Math.min(lastClickedIndex, clickedIndex);
        var endId   = Math.max(lastClickedIndex, clickedIndex);

        for (i=startId; i<=endId; i++) {
            checkBoxes[i].checked = clickedCheckbox.checked;
        }
    }

    // Store the new checkbox index as the last clicked one
    lastClickedIndexes[clickedCheckbox.name] = clickedIndex;

    // Show how many items are selected
    showCheckedCount(clickedCheckbox.name);
}

/**
 * Finds all checkboxes with the same name on the page and sets their checked
 * state based on the value of 'checked' passed in. Designed to be used as the
 * onclick handler for a "Check All" checkbox. It would be called like this:
 * <input type="checkbox" onclick="checkAll('myCheckboxes', this.checked);" />
 */
function checkAll(name, checked) {
    var checkBoxes = document.getElementsByName(name);
    for (var i=0; i<checkBoxes.length; ++i) {
        checkBoxes[i].checked = checked;
    }

    showCheckedCount(name);
}

/**
 * Displays in the status bar, the number of checkboxes selected.
 */
function showCheckedCount(name) {
    var checkboxes = document.getElementsByName(name);
    var count = 0;
    for (var i=0; i<checkboxes.length; ++i) {
        if (checkboxes[i].checked == true) {
            count++;
        }
    }

    window.status = count + " item(s) selected.";
}
