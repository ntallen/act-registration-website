#! /usr/bin/perl -w
use strict;

open(my $hf, "<", "tools/HEADER_FOOTER")
  or die "Cannot locate HEADER_FOOTER: $!";

my $source = $ARGV[0] or die "Must specify input filename\n";
open(my $src, "<", $source)
  or die "Cannot locate input file $source: $!";

my $tmpname = "$source.tmp";
unlink $tmpname;

open(my $tmp, ">", $tmpname)
  or die "Cannot open temp file $tmpname: $!";

# mergetemp($tmp, $f1, $f2, $endline)
# Copies from $f1 and $f2 up until $endline is observed
# If there are differences, use the contents of $f1
# Returns non-zero if differences are observed
sub mergetemp {
  my ($tmp, $f1, $f2, $endline) = @_;
  my $saw_change = 0;
  my $line1;
  my $line2;
  do {
    $line1 = <$f1>;
    $line2 = <$f2>;
    if (!defined($line1)) {
      die "$source: EOF in f1 before '$endline'\n";
    }
    if (!defined($line2)) {
      die "$source: EOF in f2 before '$endline'\n";
    }
    print $tmp $line1;
    if ($line1 ne $line2) {
      $saw_change = 1;
      while ($line1 ne $endline) {
        $line1 = <$f1>;
        defined($line1) or die "$source: EOF in f1 before '$endline'\n";
        print $tmp $line1;
      }
      while ($line2 ne $endline) {
        $line2 = <$f2>;
        defined($line2) or die "$source: EOF in f2 before '$endline'\n";
      }
    }
  } while $line1 ne $endline;
  return $saw_change;
}

my $hf_updated = mergetemp($tmp, $hf, $src, "<!-- bodystyle -->\n");
my $bs_nonempty = mergetemp($tmp, $src, $hf, "<!-- Orchestrator: Start of Banner -->\n");
$hf_updated |= mergetemp($tmp, $hf, $src, "<!-- Orchestrator: End of Banner -->\n");
mergetemp($tmp, $src, $hf, "<!-- Orchestrator: Start of Foot -->\n");
$hf_updated |= mergetemp($tmp, $hf, $src, "</html>\n");

close($hf);
close($src);
close($tmp);

my $bs_txt = $bs_nonempty ? ", non-empty bodystyle" : "";
if ($hf_updated) {
  print "$source: header updated$bs_txt\n";
  rename $source, "$source.bak";
  rename $tmpname, $source;
} else {
  print "$source: unchanged$bs_txt\n";
  unlink $tmpname;
}
